#!/bin/bash

#: Title       : create_project
#: Date        : 2018-01-02
#: Author      : Matthew Rutter <shellscripts@matt-rutter.com>
#: Version     : 1.0
#: Description : This shell script walks the user through the quick creation of a project on a *nix system. Git version control setup is included.
#: Options     : None


function create_project()
{

# GUI dialog box for multiple choices

    choices=$(Multi_Select_List.sh "Which type of project do you want to create?" web java c++ python 'shell script' assembly)  

# The following for loop performs an action for each chosen choice

    for choice in $choices
    do 

    # Case list allowing programmer to customize how each project type is made

        case $choice in

            1) #web project
            project_type='web'
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")
            project_dir="/home/$USER/projects/$project_type/$project_name"
            mkdir $project_dir
            cd $project_dir
            cat ~/projects/templates/$project_type/default > $project_dir/"$project_name.html"
            ;;

            2) #java project
            project_type='java'
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")
            c
            project_dir="/home/$USER/projects/$project_type/$project_name"
            mkdir "$project_dir"
            cd $project_dir
            cat ~/projects/templates/$project_type/default > $project_dir/"$project_name.java"
            ;;

            3) #cpp project
            project_type='c++'
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")
            project_dir="/home/$USER/projects/$project_type/$project_name"
            mkdir $project_dir
            cd $project_dir
            cat ~/projects/templates/$project_type/default > $project_dir/"$project_name.cpp"
            ;;

            4) #python project
            project_type='python'
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")
            project_dir="/home/$USER/projects/$project_type/$project_name"
            mkdir $project_dir
            cd $project_dir
            cat ~/projects/templates/$project_type/default > $project_dir/"$project_name.py"
            ;;

            5) #shell project
            project_type='shell'
            project_folder=$(Get_User_Input.sh "What is the $project_type project directory name?")
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")            
            project_dir="/home/$USER/projects/$project_type/"$project_folder""
            mkdir "$project_dir"
            troubleshoot_box $project_dir
            Create_Shell_File.sh "$project_dir"/"$project_name.sh"
            ln "$project_dir/"$project_name.sh"" ~/bin/$project_name                                                           #direct link the bash script in the project directory to the home bin for easy execution
            chmod 755 ~/bin/$project_name
            ;;

            6) #assembly project
            project_type='assembly'
            project_name=$(Get_User_Input.sh "What is the $project_type project name?")
            project_dir="/home/$USER/projects/$project_type/$project_name"
            mkdir $project_dir
            cd $project_dir
            cat ~/projects/templates/$project_type/default > $project_dir/"$project_name.asm"
            ;; 

        esac

        
        version_control=$(Yes_or_No.sh 'Would you like to setup a git repository for this project?')

        if [ $version_control = 0 ]
        then
            Create_Repository.sh $project_dir
        fi

    done

}

create_project
clear